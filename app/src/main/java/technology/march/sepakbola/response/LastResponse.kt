package technology.march.sepakbola.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import technology.march.sepakbola.model.LastMatch

@Parcelize
data class LastResponse(
        @SerializedName("events")
        var data: List<LastMatch>
) : Parcelable