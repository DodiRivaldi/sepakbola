package technology.march.sepakbola.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import technology.march.sepakbola.model.NextMatch

@Parcelize
data class NextResponse (
        @SerializedName("events")
        var data: List<NextMatch>
):Parcelable