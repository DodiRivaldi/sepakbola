package technology.march.sepakbola.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import technology.march.sepakbola.model.Match

@Parcelize
data class MatchResponse(
        @SerializedName("events")
        var data: List<Match>
) : Parcelable