package technology.march.sepakbola.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import technology.march.sepakbola.model.Team

@Parcelize
data class TeamResponse(
        @SerializedName("teams")
        var data: List<Team>
) : Parcelable