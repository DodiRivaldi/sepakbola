package technology.march.sepakbola.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Team(
        @SerializedName("strTeamBadge")
        var teamBadge: String? = null
) : Parcelable