package technology.march.sepakbola.view.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import technology.march.sepakbola.R
import technology.march.sepakbola.model.Match
import technology.march.sepakbola.model.Team
import technology.march.sepakbola.presenter.MatchPresenter
import technology.march.sepakbola.presenter.TeamPresenter
import technology.march.sepakbola.util.Tools
import technology.march.sepakbola.util.invisible
import technology.march.sepakbola.util.visible
import technology.march.sepakbola.view.home.HomeContract

class DetailActivity : AppCompatActivity(),
        HomeContract.ViewMatch, HomeContract.ViewTeam {

    private lateinit var progressBar: ProgressBar
    private lateinit var teamPresenter: TeamPresenter
    private lateinit var matchPresenter: MatchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DetailUi().setContentView(this)

        matchPresenter = MatchPresenter(this)
        teamPresenter = TeamPresenter(this)

        progressBar = find(R.id.pb_loading)

        val i = intent
        val idEvent: String? = i.getStringExtra("idEvent")
        val idHomeTeam: String? = i.getStringExtra("idHomeTeam")
        val idAwayTeam: String? = i.getStringExtra("idAwayTeam")
        val eventName: String? = i.getStringExtra("eventName")

        matchPresenter.showData(idEvent)
        teamPresenter.showHomeBadge(idHomeTeam)
        teamPresenter.showAwayBadge(idAwayTeam)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = eventName
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showMatch(data: List<Match>) {

        val timeEvent = find<TextView>(R.id.tv_time_event)
        val awayTeam = find<TextView>(R.id.tv_away_team)
        val dateEvent = find<TextView>(R.id.tv_date_event)
        val homeScore = find<TextView>(R.id.tv_home_score)
        val awayScore = find<TextView>(R.id.tv_away_score)
        val homeTeam = find<TextView>(R.id.tv_home_team)
        val tvVs = find<TextView>(R.id.tv_vs_match)
        val homeFormation = find<TextView>(R.id.tv_home_formation)
        val awayFormation = find<TextView>(R.id.tv_away_formation)
        val tvGoals = find<TextView>(R.id.tv_goals)
        val tvShots = find<TextView>(R.id.tv_shots)
        val homeShots = find<TextView>(R.id.tv_home_shots)
        val awayShots = find<TextView>(R.id.tv_away_shots)
        val homeGoals = find<TextView>(R.id.tv_home_goals)
        val awayGoals = find<TextView>(R.id.tv_away_goals)
        val tvLineups = find<TextView>(R.id.tv_lineups)
        val tvKeeper = find<TextView>(R.id.tv_keeper)
        val homeKeeper = find<TextView>(R.id.tv_home_keeper)
        val awayKeeper = find<TextView>(R.id.tv_away_keeper)
        val tvDefense = find<TextView>(R.id.tv_defense)
        val homeDefense = find<TextView>(R.id.tv_home_defense)
        val awayDefense = find<TextView>(R.id.tv_away_defense)
        val tvMidfield = find<TextView>(R.id.tv_midfield)
        val homeMidfield = find<TextView>(R.id.tv_home_midfield)
        val awayMidfield = find<TextView>(R.id.tv_away_midfield)
        val tvForwoard = find<TextView>(R.id.tv_forwoard)
        val homeForwoard = find<TextView>(R.id.tv_home_forwoard)
        val awayForwoard = find<TextView>(R.id.tv_away_forwoard)
        val tvSubstitutes = find<TextView>(R.id.tv_substitutes)
        val homeSubstitutes = find<TextView>(R.id.tv_home_substitutes)
        val awaySubstitutes = find<TextView>(R.id.tv_away_substitutes)
        val tvRedCard = find<TextView>(R.id.tv_red_card)
        val homeRedCard = find<TextView>(R.id.tv_home_red_card)
        val awayRedCard = find<TextView>(R.id.tv_away_red_card)
        val tvYellowCard = find<TextView>(R.id.tv_yellow_card)
        val homeYellowCard = find<TextView>(R.id.tv_home_yellow_card)
        val awayYellowCard = find<TextView>(R.id.tv_away_yellow_card)

        data.forEach {
            dateEvent.text = Tools.formatDate(it.dateEvent)
            timeEvent.text = Tools.formatTime(it.timeEvent)
            homeScore.text = it.homeScore
            awayScore.text = it.awayScore
            homeTeam.text = it.homeTeam
            awayTeam.text = it.awayTeam
            homeFormation.text = it.homeFormation
            awayFormation.text = it.awayFormation
            homeGoals.text = Tools.splitData(it.homeGoal)
            awayGoals.text = Tools.splitData(it.awayGoal)
            homeShots.text = it.homeShots
            awayShots.text = it.awayShots
            homeKeeper.text = Tools.splitData(it.homeKeeper)
            awayKeeper.text = Tools.splitData(it.awayKeeper)
            homeDefense.text = Tools.splitData(it.homeDefense)
            awayDefense.text = Tools.splitData(it.awayDefense)
            homeMidfield.text = Tools.splitData(it.homeMidfield)
            awayMidfield.text = Tools.splitData(it.awayMidfield)
            homeForwoard.text = Tools.splitData(it.homeFordward)
            awayForwoard.text = Tools.splitData(it.awayFordward)
            homeSubstitutes.text = Tools.splitData(it.homeSubstitutes)
            awaySubstitutes.text = Tools.splitData(it.awaySubstitutes)
            homeRedCard.text = Tools.splitData(it.homeRedCard)
            awayRedCard.text = Tools.splitData(it.homeRedCard)
            homeYellowCard.text = Tools.splitData(it.homeYellowCard)
            awayYellowCard.text = Tools.splitData(it.awayYellowCard)
        }
        tvVs.text = getString(R.string.vs)
        tvGoals.text = getString(R.string.goals)
        tvShots.text = getString(R.string.shoots)
        tvLineups.text = getString(R.string.lineup)
        tvKeeper.text = getString(R.string.gk)
        tvDefense.text = getString(R.string.df)
        tvMidfield.text = getString(R.string.mf)
        tvForwoard.text = getString(R.string.cf)
        tvSubstitutes.text = getString(R.string.subs)
        tvYellowCard.text = getString(R.string.ycard)
        tvRedCard.text = getString(R.string.redcard)
    }

    override fun showHomeBadge(data: List<Team>) {
        val homeBadge: ImageView = find(R.id.iv_home_badge)
        data.forEach {
            Picasso.get().load(it.teamBadge).into(homeBadge)
        }
    }

    override fun showAwayBadge(data: List<Team>) {
        val awayBadge: ImageView = find(R.id.iv_away_badge)
        data.forEach {
            Picasso.get().load(it.teamBadge).into(awayBadge)
        }
    }
}