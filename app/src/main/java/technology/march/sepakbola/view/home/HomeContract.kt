package technology.march.sepakbola.view.home

import technology.march.sepakbola.model.LastMatch
import technology.march.sepakbola.model.Match
import technology.march.sepakbola.model.NextMatch
import technology.march.sepakbola.model.Team

interface HomeContract {
    interface Presenter{
        fun showData()
    }

    interface MatchPresenter{
        fun showData(idEvent: String?)
    }

    interface TeamPresenter{
        fun showHomeBadge(idTeam: String?)
        fun showAwayBadge(idTeam: String?)
    }

    interface ViewLast{
        fun showLoading()
        fun hideLoading()
        fun showLastMatch(data:List<LastMatch>)
    }

    interface ViewNext{
        fun showLoading()
        fun hideLoading()
        fun showNextMatch(data:List<NextMatch>)
    }

    interface ViewTeam{
        fun showHomeBadge(data:List<Team>)
        fun showAwayBadge(data:List<Team>)
    }

    interface ViewMatch{
        fun showLoading()
        fun hideLoading()
        fun showMatch(data:List<Match>)
    }
}