package technology.march.sepakbola.view.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import technology.march.sepakbola.R
import technology.march.sepakbola.view.fragment.LastFragment
import technology.march.sepakbola.view.fragment.NextFragment

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_sebelum -> {
                    loadLastMatchFragment(savedInstanceState)
                }
                R.id.nav_selanjutnya -> {
                    loadNextMatchFragment(savedInstanceState)
                }
            }
            true
        }
        navigation.selectedItemId = R.id.nav_sebelum
    }

    private fun loadLastMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fl_container, LastFragment(), LastFragment::class.simpleName)
                    .commit()
        }
    }

    private fun loadNextMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fl_container, NextFragment(), NextFragment::class.simpleName)
                    .commit()
        }
    }
}
