package technology.march.sepakbola.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import technology.march.sepakbola.R
import technology.march.sepakbola.model.NextMatch
import technology.march.sepakbola.model.Team
import technology.march.sepakbola.presenter.TeamPresenter
import technology.march.sepakbola.util.Tools
import technology.march.sepakbola.view.match.MatchUi
import technology.march.sepakbola.view.home.HomeContract

class NextAdapter (private val nextmatch: List<NextMatch>, private val listener: (NextMatch)->Unit)
    : RecyclerView.Adapter<NextMatchViewHolver>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextMatchViewHolver {
        return NextMatchViewHolver(MatchUi().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: NextMatchViewHolver, position: Int) {
        holder.bindItem(nextmatch[position], listener)
    }

    override fun getItemCount(): Int = nextmatch.size
}

class NextMatchViewHolver(view: View): RecyclerView.ViewHolder(view), HomeContract.ViewTeam {

    private val dateEvent = view.find<TextView>(R.id.tv_date_event)
    private val homeTeam = view.find<TextView>(R.id.tv_home_team)
    private val awayTeam = view.find<TextView>(R.id.tv_away_team)
    private val homeScore = view.find<TextView>(R.id.tv_home_score)
    private val awayScore = view.find<TextView>(R.id.tv_away_score)
    private val homeBadge: ImageView = view.find(R.id.iv_home_badge)
    private val awayBadge: ImageView = view.find(R.id.iv_away_badge)
    private lateinit var teamPresenter: TeamPresenter

    fun bindItem(nextmatch: NextMatch, listener: (NextMatch) -> Unit){

        dateEvent.text = Tools.formatDate(nextmatch.dateEvent)
        homeTeam.text = nextmatch.homeTeam
        awayTeam.text = nextmatch.awayTeam
        homeScore.text = nextmatch.homeScore
        awayScore.text = nextmatch.awayScore

        val idHomeTeam: String? = nextmatch.idHomeTeam
        val idAwayTeam: String? = nextmatch.idAwayTeam

        itemView.setOnClickListener{listener(nextmatch)}

        teamPresenter = TeamPresenter(this)
        teamPresenter.showHomeBadge(idHomeTeam)
        teamPresenter.showAwayBadge(idAwayTeam)

    }

    override fun showHomeBadge(data: List<Team>) {
        data.forEach {
            Picasso.get().load(it.teamBadge).into(homeBadge)
        }
    }

    override fun showAwayBadge(data: List<Team>) {
        data.forEach {
            Picasso.get().load(it.teamBadge).into(awayBadge)
        }
    }
}
