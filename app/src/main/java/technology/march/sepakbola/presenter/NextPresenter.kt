package technology.march.sepakbola.presenter

import retrofit2.Call
import retrofit2.Response
import technology.march.sepakbola.BuildConfig
import technology.march.sepakbola.api.ApiClient
import technology.march.sepakbola.response.NextResponse
import technology.march.sepakbola.view.home.HomeContract

class NextPresenter (private val view: HomeContract.ViewNext) : HomeContract.Presenter {

    private var API_KEY: String? = BuildConfig.TSDB_API_KEY
    private var LEAGUE_ID: String? = "4328"

    override fun showData(){

        view.showLoading()

        val call = ApiClient().getInstance().getNextMatch(
                API_KEY,LEAGUE_ID
        )

        call.enqueue(object : retrofit2.Callback<NextResponse>{

            override fun onFailure(call: Call<NextResponse>?, t: Throwable?){

                view.hideLoading()
            }

            override fun onResponse(call: Call<NextResponse>?, response: Response<NextResponse>?){
                if (response != null){
                    if (response.isSuccessful){

                        val data = response.body()!!.data

                        view.showNextMatch(data)
                        view.hideLoading()
                    }else{
                        view.hideLoading()
                    }
                }
            }
        })
    }
}