package technology.march.sepakbola.presenter

import retrofit2.Call
import retrofit2.Response
import technology.march.sepakbola.BuildConfig
import technology.march.sepakbola.api.ApiClient
import technology.march.sepakbola.response.LastResponse
import technology.march.sepakbola.view.home.HomeContract

class LastPresenter (private val view: HomeContract.ViewLast) : HomeContract.Presenter {

    private var API_KEY: String? = BuildConfig.TSDB_API_KEY
    private var LEAGUE_ID: String? = "4328"

    override fun showData(){

        view.showLoading()

        val call = ApiClient().getInstance().getLastMatch(
                API_KEY,LEAGUE_ID
        )

        call.enqueue(object : retrofit2.Callback<LastResponse>{

            override fun onFailure(call: Call<LastResponse>?, t: Throwable?){

                view.hideLoading()
            }

            override fun onResponse(call: Call<LastResponse>?, response: Response<LastResponse>?){
                if (response != null){
                    if (response.isSuccessful){

                        val data = response.body()!!.data

                        view.showLastMatch(data)
                        view.hideLoading()
                    }else{
                        view.hideLoading()
                    }
                }
            }
        })
    }
}