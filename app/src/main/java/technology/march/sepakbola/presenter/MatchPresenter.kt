package technology.march.sepakbola.presenter

import retrofit2.Call
import retrofit2.Response
import technology.march.sepakbola.BuildConfig
import technology.march.sepakbola.api.ApiClient
import technology.march.sepakbola.response.MatchResponse
import technology.march.sepakbola.view.home.HomeContract

class MatchPresenter (private val view: HomeContract.ViewMatch) : HomeContract.MatchPresenter {

    private var API_KEY: String? = BuildConfig.TSDB_API_KEY

    override fun showData(idEvent: String?){

        view.showLoading()

        val call = ApiClient().getInstance().getMatch(
                API_KEY,idEvent
        )

        call.enqueue(object : retrofit2.Callback<MatchResponse>{

            override fun onFailure(call: Call<MatchResponse>?, t: Throwable?){

                view.hideLoading()
            }

            override fun onResponse(call: Call<MatchResponse>?, response: Response<MatchResponse>?){
                if (response != null){
                    if (response.isSuccessful){

                        val data = response.body()!!.data

                        view.showMatch(data)
                        view.hideLoading()
                    }else{
                        view.hideLoading()
                    }
                }
            }
        })
    }
}