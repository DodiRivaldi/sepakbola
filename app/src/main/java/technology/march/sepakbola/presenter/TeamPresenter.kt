package technology.march.sepakbola.presenter

import retrofit2.Call
import retrofit2.Response
import technology.march.sepakbola.BuildConfig
import technology.march.sepakbola.api.ApiClient
import technology.march.sepakbola.response.TeamResponse
import technology.march.sepakbola.view.home.HomeContract

class TeamPresenter (private val view: HomeContract.ViewTeam) : HomeContract.TeamPresenter {

    private var API_KEY: String? = BuildConfig.TSDB_API_KEY

    override fun showHomeBadge(idTeam: String?){

        val call = ApiClient().getInstance().getTeam(
                API_KEY,idTeam
        )

        call.enqueue(object : retrofit2.Callback<TeamResponse>{

            override fun onFailure(call: Call<TeamResponse>?, t: Throwable?){

            }

            override fun onResponse(call: Call<TeamResponse>?, response: Response<TeamResponse>?){
                if (response != null){
                    if (response.isSuccessful){

                        val data = response.body()!!.data

                        view.showHomeBadge(data)
                    }
                }
            }
        })
    }

    override fun showAwayBadge(idTeam: String?){

        val call = ApiClient().getInstance().getTeam(
                API_KEY,idTeam
        )

        call.enqueue(object : retrofit2.Callback<TeamResponse>{

            override fun onFailure(call: Call<TeamResponse>?, t: Throwable?){

            }

            override fun onResponse(call: Call<TeamResponse>?, response: Response<TeamResponse>?){
                if (response != null){
                    if (response.isSuccessful){

                        val data = response.body()!!.data

                        view.showAwayBadge(data)
                    }
                }
            }
        })
    }
}